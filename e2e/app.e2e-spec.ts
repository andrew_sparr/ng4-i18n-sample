import { I18nSamplePage } from './app.po';

describe('i18n-sample App', () => {
  let page: I18nSamplePage;

  beforeEach(() => {
    page = new I18nSamplePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
